#/bin/sh
#Quick n' Dirty macOS setup script for me to use
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
brew install wget git yt-dlp neofetch
brew  install --cask firefox steam discord xee iina qlvideo swinsian the-unarchiver rectangle calibre audacity cryptomator keepassxc krita gimp microsoft-office qbittorrent protonvpn protonmail-bridge appcleaner
wget -O .zshrc https://git.grml.org/f/grml-etc-core/etc/zsh/zshrc
# maybe set up config for root as well?
exit 0